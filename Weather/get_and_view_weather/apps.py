from django.apps import AppConfig


class GetAndViewWeatherConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'get_and_view_weather'
